﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_MVVM_HotelReservationSys.DbContext;
using WPF_MVVM_HotelReservationSys.DTOs;
using WPF_MVVM_HotelReservationSys.Models;

namespace WPF_MVVM_HotelReservationSys.ReservationConflictValidators
{
    public class DatabaseReservationConflictValidator : IReservationConflictValidator
    {
        private readonly HRSDbContextFactory _dbContextFactory;

        public DatabaseReservationConflictValidator(HRSDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<Reservation> GetConflictingReservation(Reservation reservation)
        {
            using (HRSDbContext db = _dbContextFactory.CreateDbContext())
            {
                /*return await db.Reservations
                    .Select(r => ToReservation(r))
                    .FirstOrDefaultAsync(r => r.Conflicts(reservation));*/
                ReservationDTO? reservationDTO = await db.Reservations
                    .Where(r => r.FloorNumber == reservation.RoomID.FloorNumber)
                    .Where(r => r.RoomNumber == reservation.RoomID.RoomNumber)
                    .Where(r => r.EndTime > reservation.EndTime)
                    .Where(r => r.StartTime < reservation.StartTime)
                    .FirstOrDefaultAsync();

                if (reservationDTO == null)
                {
                    return null;
                }

                return ToReservation(reservationDTO);
            }
        }

        private static Reservation ToReservation(ReservationDTO r)
        {
            return new Reservation(new RoomID(r.FloorNumber, r.RoomNumber), r.UserName, r.StartTime, r.EndTime);
        }
    }
}
