﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_MVVM_HotelReservationSys.DbContext;
using WPF_MVVM_HotelReservationSys.DTOs;
using WPF_MVVM_HotelReservationSys.Models;

namespace WPF_MVVM_HotelReservationSys.Services.ReservationProviders
{
    public class DatabaseReservationProvider : IReservationProvider
    {
        private readonly HRSDbContextFactory _dbContextFactory;

        public DatabaseReservationProvider(HRSDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<IEnumerable<Reservation>> GetReservations()
        {
            using (HRSDbContext db = _dbContextFactory.CreateDbContext())
            {
                IEnumerable<ReservationDTO> reservationsDTOs = await db.Reservations.ToListAsync();

                return reservationsDTOs.Select(r => ToReservation(r));
            }
        }

        private static Reservation ToReservation(ReservationDTO r)
        {
            return new Reservation(new RoomID(r.FloorNumber, r.RoomNumber), r.UserName, r.StartTime, r.EndTime);
        }
    }
}
