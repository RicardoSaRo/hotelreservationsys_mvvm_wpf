﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_MVVM_HotelReservationSys.Models
{
    //RoomID is separated as class to maintain the logic of ID calculation encapsulated.
    public class RoomID
    {
        public RoomID(int floorNumber, int roomNumber)
        {
            FloorNumber = floorNumber;
            RoomNumber = roomNumber;
        }

        public override string ToString()
        {
            return $"{FloorNumber}{RoomNumber}"; //--> Sorted for better use on views.
        }

        public int FloorNumber { get; }

        public int RoomNumber { get; }

        //--> Equals method needs to be overriden for the ReservationBook Key to be able 
        //--> to compare keys whenever you get or add values to the dictionary.
        public override bool Equals(object obj)
        {
            return obj is RoomID roomID &&           //--> If the other obj is a RoomID (Type Check)
                FloorNumber == roomID.FloorNumber && //--> Store RoomInfo in "roomID" to compare as
                RoomNumber == roomID.RoomNumber;     //--> well the Room number and floor.
        }

        //--> Overrides as well the equals operator for the RoomID
        //--> Remember to use "is" instead os "==" to prevent stackoverflow
        public static bool operator ==(RoomID roomID1, RoomID roomID2)
        {
            if (roomID1 is null && roomID2 is null)
            {
                return true;
            }

            return !(roomID1 is null) && roomID1.Equals (roomID2);
        }

        //--> An equals operator override needs an not-equals operator override
        public static bool operator !=(RoomID roomID1,RoomID roomID2)
        {
            return !(roomID1 == roomID2);
        }


        //--> If Object.Equals(object o) is overriden, Object.GetHashCode() must have an override as well (CS0659).
        public override int GetHashCode()
        {
            return HashCode.Combine(FloorNumber, RoomNumber); //--> Use combine to create a easy/unique hash.
        }

    }
}
