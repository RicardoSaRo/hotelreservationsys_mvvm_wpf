﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_MVVM_HotelReservationSys.Models
{
    public class Hotel
    {
        private readonly ReservationBook _reservationBook;

        public string Name { get; }

        public Hotel(string name, ReservationBook reservationBook)
        {
            Name = name;

            _reservationBook = reservationBook;
        }

        /// <summary>
        /// Get the reservations for the user
        /// </summary>
        /// <param name="username"></param>
        /// <returns>The reservations for the user</returns>
        public async Task<IEnumerable<Reservation>> GetAllReservations()
        {
            return await _reservationBook.GetAllReservations();
        }

        /// <summary>
        /// Make Reservation
        /// </summary>
        /// <param name="reservation">The incoming reservation</param>
        /// <exception cref="ReservationConflictException"
        public async Task MakeReservation(Reservation reservation)
        {
            await _reservationBook.AddReservation(reservation);
        }
    }
}

//--> Made most readonly mostly because all models are mapped and changing one attribute
//--> like RoomID or dates, could make conflicts between reservations.
