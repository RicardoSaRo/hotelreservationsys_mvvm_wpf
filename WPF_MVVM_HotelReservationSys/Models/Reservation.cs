﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_MVVM_HotelReservationSys.Models
{
    public class Reservation
    {
        public Reservation(RoomID roomID, string userName, DateTime startTime, DateTime endTime)
        {
            RoomID = roomID;
            UserName = userName;
            StartTime = startTime;
            EndTime = endTime;
        }

        public RoomID RoomID { get; }

        public string UserName { get; }

        public DateTime StartTime { get; }

        public DateTime EndTime { get; }

        public TimeSpan Lenght => EndTime.Subtract(StartTime);
    }
}
