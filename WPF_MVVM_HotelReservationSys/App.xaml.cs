﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WPF_MVVM_HotelReservationSys.DbContext;
using WPF_MVVM_HotelReservationSys.Exceptions;
using WPF_MVVM_HotelReservationSys.Models;
using WPF_MVVM_HotelReservationSys.ReservationConflictValidators;
using WPF_MVVM_HotelReservationSys.Services;
using WPF_MVVM_HotelReservationSys.Services.ReservationCreators;
using WPF_MVVM_HotelReservationSys.Services.ReservationProviders;
using WPF_MVVM_HotelReservationSys.Stores;
using WPF_MVVM_HotelReservationSys.ViewModels;

namespace WPF_MVVM_HotelReservationSys
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly Hotel _hotel;
        private readonly NavigationStore _navigationStore;
        private readonly HRSDbContextFactory _hrsDbContextFactory;
        private const string CONNECTION_STRING = "Data Source = HotelReservSys";

        public App()
        {
            _hrsDbContextFactory = new HRSDbContextFactory(CONNECTION_STRING);
            IReservationProvider reservationProvider = new DatabaseReservationProvider(_hrsDbContextFactory);
            IReservationCreator? reservationCreator = new DatabaseReservationCreator(_hrsDbContextFactory);
            IReservationConflictValidator? reservationConflictValidator = new DatabaseReservationConflictValidator(_hrsDbContextFactory);
            ReservationBook reservationBook = new ReservationBook(reservationProvider,reservationCreator,reservationConflictValidator);
            _hotel = new Hotel("RSR Inn",);
            _navigationStore = new NavigationStore();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            //--> Generates DataBase in the bin
            //DbContextOptions options = new DbContextOptionsBuilder().UseSqlite(CONNECTION_STRING).Options;
            //--> The dbcontext is not a long-lived object... must be disposed after the app closes.
            //--> So I have to use "using"
            using (HRSDbContext db = _hrsDbContextFactory.CreateDbContext())
            {
                db.Database.Migrate();
            };

            _navigationStore.CurrentViewModel = CreateReservationViewModel();

            MainWindow = new MainWindow()
            {
                DataContext = new MainViewModel(_navigationStore)
            };
            MainWindow.Show();

            base.OnStartup(e);
        }

        private MakeReservationViewModel CreateMakeReservationViewModel()
        {
            return new MakeReservationViewModel(_hotel, new NavigationService(_navigationStore, CreateReservationViewModel));
        }

        private ReservationListingViewModel CreateReservationViewModel()
        {
            return ReservationListingViewModel.LoadViewModel(_hotel, new NavigationService(_navigationStore, CreateMakeReservationViewModel));
        }
    }
}
