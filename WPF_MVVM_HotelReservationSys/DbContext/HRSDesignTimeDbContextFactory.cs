﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_MVVM_HotelReservationSys.DbContext
{
    
    public class HRSDesignTimeDbContextFactory : IDesignTimeDbContextFactory<HRSDbContext>
    {
        public HRSDbContext CreateDbContext(string[] args)
        {
            DbContextOptions options = new DbContextOptionsBuilder().UseSqlite("Data Source = HotelReservSys").Options;

            return new HRSDbContext(options);
        }
    }
}
