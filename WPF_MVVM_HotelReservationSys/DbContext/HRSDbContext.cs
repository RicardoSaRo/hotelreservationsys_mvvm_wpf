﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_MVVM_HotelReservationSys.Models;
//using Microsoft.EntityFrameworkCore;

namespace WPF_MVVM_HotelReservationSys.DbContext
{
    using Microsoft.EntityFrameworkCore; //--> For some reason, if I dont use this here DbContext cannot be used 
    using WPF_MVVM_HotelReservationSys.DTOs;

    public class HRSDbContext : DbContext
    {
        public HRSDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<ReservationDTO> Reservations { get; set; }
    }
}
