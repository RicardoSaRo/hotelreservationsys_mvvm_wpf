﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_MVVM_HotelReservationSys.DbContext
{
    public class HRSDbContextFactory
    {
        private readonly string _connectionString;

        public HRSDbContextFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public HRSDbContext CreateDbContext()
        {
            DbContextOptions options = new DbContextOptionsBuilder().UseSqlite(_connectionString).Options;

            return new HRSDbContext(options);
        }
    }
}
